package main

import (
	"crypto/sha256"
	"fmt"
	"io/ioutil"
	"log"
)

func main() {
	img := []byte(";image_1.jpg")
	content, err := ioutil.ReadFile("tst",img)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("File contents: %s", content)
}
