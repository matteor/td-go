package main

import (
    "encoding/json"
    "fmt"
    "log"
    "net/http"
	"strconv"
	"io/ioutil"
)

type Task struct {
    Description string `json:"description"`
    Done        bool `json:"done"`
}

type List struct {
	ID   string
	Task string
}

var tasks []Task

func main() {
   	http.HandleFunc("/", list)
	http.HandleFunc("/done", done)
	http.HandleFunc("/add", add)
    http.ListenAndServe(":8080", nil)
}

func list(rw http.ResponseWriter, _ *http.Request) {
	list := []List{}
	for id, n1 := range tasks {
		if !n1.Done {
			list = append(list, List{strconv.Itoa(id), n1.Description})
		}
	}
	rw.WriteHeader(http.StatusOK)
	data, _ := json.Marshal(list)

	rw.Write(data)
}

func add(rw http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Printf("Error reading body: %v", err)
		http.Error(rw,"can't read body", http.StatusBadRequest,)
		return
	}
	description := string(body)
	tasks = append(tasks,Task{description, false})
}

func done(rw http.ResponseWriter, r *http.Request) {
// fonction case
	switch r.Method{
	case http.MethodGet:
		fmt.Println("Get")
		rw.WriteHeader(http.StatusOK)
	case http.MethodPost:
		monid := r.FormValue("id")
		id,_ := strconv.Atoi(monid)
		if id > len(tasks)-1{
			rw.WriteHeader(http.StatusBadRequest)
		}
		for n2, _ := range tasks {
			if n2 == id {
				tasks[n2] = Task{monid,true}
			}
		}
		rw.WriteHeader(http.StatusOK)
	default:
		rw.WriteHeader(http.StatusBadRequest)
	}
}
