package main

import (
    "flag"
    "fmt"
)

const VERSION = "1.0"

func main() {
    var version = flag.Bool("version", false, "Demandez version du programme")
    flag.Parse()
    
    if *version {
        fmt.Println(VERSION)
    }
}
